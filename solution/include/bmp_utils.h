#ifndef LAB3_BMP_UTILS_H
#define LAB3_BMP_UTILS_H

#include "image_utils.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_HEADER_ERROR,
    READ_IMAGE_ERROR
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

static const char* read_status_string[] = {
        [READ_OK] = "Reading successfully.",
        [READ_INVALID_SIGNATURE] = "Error in bmp file type.",
        [READ_INVALID_BITS] = "Error bmp file not 24 bit.",
        [READ_HEADER_ERROR] = "Error when reading bmp header.",
        [READ_IMAGE_ERROR] = "Error when reading image from bmp file.",
};

static const char* write_status_string[] = {
        [WRITE_OK] = "Writing successfully.",
        [WRITE_ERROR] = "Error when image writing to bmp.",
};

enum read_status from_bmp(FILE* file, struct image** img);

enum write_status to_bmp(FILE* file, struct image const* img);

#endif //LAB3_BMP_UTILS_H
