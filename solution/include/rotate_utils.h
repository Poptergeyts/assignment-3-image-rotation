#ifndef LAB3_ROTATE_UTILS_H
#define LAB3_ROTATE_UTILS_H

#include "image_utils.h"

#include <stdio.h>

enum rotate_status {
    ROTATE_OK = 0,
    ERROR_ROTATE,
    INVALID_ANGLE
};

static const char* rotate_status_string[] = {
        [ROTATE_OK] = "Image rotated successfully.",
        [ERROR_ROTATE] = "Rotation error.",
        [INVALID_ANGLE] = "Invalid angle.",
};

enum rotate_status rotate(struct image** img);

#endif //LAB3_ROTATE_UTILS_H
