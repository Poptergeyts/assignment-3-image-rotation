#ifndef LAB3_FILE_UTILS_H
#define LAB3_FILE_UTILS_H

#include <stdio.h>

enum file_open_status {
    FILE_OPEN_OK = 0,
    FILE_OPEN_ERROR,
    FILE_OPEN_READ_ERROR,
    FILE_OPEN_WRITE_ERROR

};

static const char* file_open_status_string[] = {
    [FILE_OPEN_OK] = "File is open.",
    [FILE_OPEN_ERROR] = "Error when opening the file.",
    [FILE_OPEN_READ_ERROR] = "Error when opening the file for reading.",
    [FILE_OPEN_WRITE_ERROR] = "Error when opening the file for writing."
};

enum file_open_status open_file_for_reading(const char* filename, FILE** file);

enum file_open_status open_file_for_writing(const char* filename, FILE** file);

#endif //LAB3_FILE_UTILS_H
