#ifndef LAB3_IMAGE_UTILS_H
#define LAB3_IMAGE_UTILS_H

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t red, green, blue;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image* new_image(uint32_t width, uint32_t height);

void delete_image(struct image* img);

#endif //LAB3_IMAGE_UTILS_H
