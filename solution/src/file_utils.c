#include "file_utils.h"

enum file_open_status open_file_for_reading(const char* filename, FILE** file) {
    *file = fopen(filename, "rb");
    if (!*file) {
        return FILE_OPEN_READ_ERROR;
    }
    return FILE_OPEN_OK;
}

enum file_open_status open_file_for_writing(const char* filename, FILE** file) {
    *file = fopen(filename, "wb");
    if (!*file) {
        return FILE_OPEN_WRITE_ERROR;
    }
    return FILE_OPEN_OK;
}

