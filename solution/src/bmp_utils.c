#define LITTLE_ENDING 0x4D42

#include "bmp_utils.h"

static enum read_status read_bmp_header(FILE* file, struct bmp_header* header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        return READ_HEADER_ERROR;
    }
    return READ_OK;
}

static int8_t get_padding(uint64_t width) {
    return (int8_t)((4 - (width * sizeof(struct pixel)) % 4) % 4);
}

static struct bmp_header new_bmp_header(const struct image* img) {
    uint32_t image_width = img -> width;
    uint32_t image_height = img -> height;
    int8_t padding = get_padding(img -> width);
    return (struct bmp_header) {
            .bfType = LITTLE_ENDING,
            .bfileSize = sizeof(struct bmp_header) +
                         (image_width + padding) * image_height * sizeof(struct pixel),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image_width,
            .biHeight = image_height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (image_width + padding) * image_height * sizeof(struct pixel),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE* file, struct image** img) {
    struct bmp_header header = {0};
    if (read_bmp_header(file, &header)) {
        return READ_HEADER_ERROR;
    }
    if (header.bfType != LITTLE_ENDING) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;
    (*img) = new_image(width, height);
    fseek(file, header.bOffBits, SEEK_SET);
    if (!((*img) -> data)) {
        return READ_IMAGE_ERROR;
    }
    int8_t padding = get_padding(width);
    for (size_t i = 0; i < height; i++) {
        if (fread(&((*img) -> data[(height - 1 - i) * width]), sizeof(struct pixel), width, file) != width) {
            return READ_IMAGE_ERROR;
        }
        fseek(file, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* file, const struct image* img) {
    uint32_t image_width = img -> width;
    uint32_t image_height = img -> height;
    int8_t padding = get_padding(img -> width);
    uint8_t zero[4] = {0};

    struct bmp_header header = new_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < image_height; i++) {
        if (fwrite(&(img -> data[(image_height - 1 - i) * image_width]),
                  sizeof(struct pixel), image_width, file) != image_width) {
            return WRITE_ERROR;
        }
        if (fwrite(zero, sizeof(uint8_t ), padding, file) != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
