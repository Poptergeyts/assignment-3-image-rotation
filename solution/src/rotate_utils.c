#include "rotate_utils.h"
#include "image_utils.h"

enum rotate_status rotate(struct image** img) {
    uint32_t image_width = (*img) -> width;
            uint32_t image_height = (*img) -> height;
            struct image* rotated_image = new_image(image_height, image_width);
            for (size_t i = 0; i < image_width; i++) {
                for (size_t j = 0; j < image_height; j++) {
                    rotated_image -> data[image_height * i + j] =
                            (*img) -> data[image_width * (image_height - j - 1) + i];
                }
            }
            delete_image((*img));
            (*img) = rotated_image;
            return ROTATE_OK;
}
