#include "image_utils.h"

struct image* new_image(uint32_t width, uint32_t height) {
    struct image* img = malloc(sizeof(struct image));
    struct pixel* data_ptr = malloc(sizeof(struct pixel) * width * height);
    img -> width = width;
    img -> height = height;
    img -> data = data_ptr;
    return img;
}

void delete_image(struct image* img) {
    if (img) {
        if (img -> data) {
            free(img->data);
        }
        free(img);
    }
}
