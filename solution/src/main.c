#include "image_utils.h"
#include "bmp_utils.h"
#include "file_utils.h"
#include "rotate_utils.h"

#ifndef STDIO_H
#define STDIO_H
#include <stdio.h>
#endif //STDIO_H

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Wrong number of input arguments.");
        return -1;
    }

    const char* file_for_reading_filename = argv[1];
    const char* file_for_writing_filename = argv[2];
    int angle = atoi(argv[3]);
    FILE* file_for_reading = NULL;
    enum file_open_status file_for_reading_open_status =
            open_file_for_reading(file_for_reading_filename, &file_for_reading);
    if (file_for_reading_open_status) {
        fprintf(stderr, "%s", file_open_status_string[file_for_reading_open_status]);
        return file_for_reading_open_status;
    }
    struct image* image = NULL;
    enum read_status bmp_read_status = from_bmp(file_for_reading, &image);
    if (bmp_read_status) {
        fprintf(stderr, "%s", read_status_string[bmp_read_status]);
        delete_image(image);
        return bmp_read_status;
    }
    fclose(file_for_reading);

    enum rotate_status image_rotate_status;
    if (angle % 90) {
        fprintf(stderr, "%s", rotate_status_string[INVALID_ANGLE]);
    }
    if (angle < 90) {
        angle += 360;
    }
    while (angle) {
        image_rotate_status = rotate(&image);
        if (image_rotate_status) {
            fprintf(stderr, "%s", rotate_status_string[image_rotate_status]);
            delete_image(image);
            return image_rotate_status;
        }
        angle -= 90;
    }

    FILE* file_for_writing = NULL;
    enum file_open_status file_for_writing_open_status =
            open_file_for_writing(file_for_writing_filename, &file_for_writing);
    if (file_for_writing_open_status) {
        fprintf(stderr, "%s", file_open_status_string[file_for_writing_open_status]);
        delete_image(image);
        return file_for_writing_open_status;
    }
    enum write_status bmp_write_status = to_bmp(file_for_writing, image);
    if (bmp_write_status) {
        fprintf(stderr, "%s", write_status_string[bmp_write_status]);
        delete_image(image);
        return bmp_read_status;
    }
    fclose(file_for_writing);
    delete_image(image);
    printf("Success.");
    return 0;
}
